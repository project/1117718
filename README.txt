
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Usage

INTRODUCTION
------------

Current Maintainer: mobifilia
Profile: http://drupal.org/user/1256140
Contact: http://drupal.org/user/1256140/contact

INSTALLATION
------------

1. Download and extract the module to [yourdrupalroot]/sites/all/modules
(final structure will be [yourdrupalroot]/sites/all/modules/foursqure_tips


USAGE
-----
1. Go to Block Configuration
2. Enter Latitude
3. Enter Logitude
4. Enter Limit
5. Enter Foursquare Token Key
6. Save.
7. Set Block region.
